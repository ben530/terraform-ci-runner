bucket = "wl-terraform-state-prod"

key = "wpfma-infrastructure/prod.tfstate"

dynamodb_table = "wl-terraform-locks-prod"

region = "us-east-1"
