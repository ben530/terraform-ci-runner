output "cognito_user_pool_id" {
  value = tolist(data.aws_cognito_user_pools.nea.ids)[0]
}

output "cognito_api_client_read_id" {
  value = "${module.cognito-api-client-read.cognito_client_id}"
}

output "cognito_api_client_read_secret_arn" {
  value = "${module.cognito-api-client-read.secretsmanager_secret_arn}"
}

output "cognito_api_client_write_id" {
  value = "${module.cognito-api-client-write.cognito_client_id}"
}

output "cognito_api_client_write_secret_arn" {
  value = "${module.cognito-api-client-write.secretsmanager_secret_arn}"
}

output "cognito_ui_client_id" {
  value = "${module.cognito-ui-client.cognito_client_id}"
}

output "database_name" {
  value = "${module.rds-aurora-serverless.rds_cluster_database_name}"
}

output "database_secret_arn" {
  value = "${module.rds-aurora-serverless.secretsmanager_secret_arn}"
}

output "rds_client_security_group_id" {
  value = "${module.rds-aurora-serverless.rds_client_security_group_id}"
}

output "rds_cluster_arn" {
  value = "${module.rds-aurora-serverless.rds_cluster_arn}"
}

output "rds_cluster_endpoint" {
  value = "${module.rds-aurora-serverless.rds_cluster_endpoint}"
}

output "rds_cluster_port" {
  value = "${module.rds-aurora-serverless.rds_cluster_port}"
}

output "vpc_subnet_ids" {
  value = tolist(data.aws_subnet_ids.private.ids)
}
