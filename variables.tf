# ------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ------------------------------------------------------------------
variable "app_name" {
  type        = string
  description = "Name of the application used for naming/tagging resources."
}

variable "region" {
  type        = string
  default     = "us-east-1"
  description = "AWS region being deployed to (defaults to us-east-1)."
}

variable "stage" {
  type        = string
  default     = "dev"
  description = "Environment being deployed to (ie. dev or prod)."
}
