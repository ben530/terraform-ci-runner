# ------------------------------------------------------------------
# Create Cognito UI Client for User Pool with custom IdP support
# ------------------------------------------------------------------
resource "aws_cognito_user_pool_client" "ui_client" {
  name = var.client_name

  user_pool_id = var.cognito_user_pool_id

  refresh_token_validity = 1

  generate_secret = false

  allowed_oauth_flows_user_pool_client = true

  allowed_oauth_flows = [
    "code",
  ]

  allowed_oauth_scopes = [
    "openid",
    "phone",
    "profile",
    "email",
    "aws.cognito.signin.user.admin",
  ]

  supported_identity_providers = var.identity_providers

  callback_urls = var.callback_urls

  logout_urls = var.logout_urls

  read_attributes = [
    #uncomment "custom:ad_groups",
    "email",
    "family_name",
    "given_name",
  ]

  write_attributes = [
    #uncomment "custom:ad_groups",
    "email",
    "family_name",
    "given_name",
  ]
}
