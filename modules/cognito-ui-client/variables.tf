# ------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ------------------------------------------------------------------
variable "client_name" {
  type        = string
  description = "Unique Name for the Cognito Client"
}

variable "cognito_user_pool_id" {
  type        = string
  default     = "us-east-1_rOjxrvCnU"
  description = "Cognito User Pool Id to attach the client to (defaults to nea-prod-pool)"
}

variable "identity_providers" {
  type        = list(string)
  default     = ["COGNITO"]  #uncomment, "nextera-adfs"]
  description = "List of identity providers that will be used by the client"
}

variable "callback_urls" {
  type        = list(string)
  description = "List of URLs that will be used for logging in to Cognito from the UI"
}

variable "logout_urls" {
  type        = list(string)
  description = "List of URLs that will be used for logging out of Cognito from the UI"
}

