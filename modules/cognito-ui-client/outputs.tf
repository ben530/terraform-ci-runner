output "cognito_client_id" {
  value = aws_cognito_user_pool_client.ui_client.id
}

output "cognito_client_identity_providers" {
  value = aws_cognito_user_pool_client.ui_client.supported_identity_providers
}
