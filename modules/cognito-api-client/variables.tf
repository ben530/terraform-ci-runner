# ------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ------------------------------------------------------------------
variable "client_name" {
  type        = string
  description = "Unique Name for the Cognito Client"
}

variable "cognito_user_pool_id" {
  type        = string
  default     = "us-east-1_rOjxrvCnU"
  description = "Cognito User Pool Id to attach the client to (defaults to nea-prod-pool)"
}

variable "oauth_scopes" {
  type        = list(string)
  description = "List of OAuth scopes that the client will have access to"
}
