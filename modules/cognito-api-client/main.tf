# ------------------------------------------------------------------
# Create Cognito API Client for User Pool with custom IdP support
# ------------------------------------------------------------------
resource "aws_cognito_user_pool_client" "api_client" {
  name = var.client_name

  user_pool_id = var.cognito_user_pool_id

  refresh_token_validity = 1

  generate_secret = true

  allowed_oauth_flows_user_pool_client = true

  allowed_oauth_flows = [
    "client_credentials",
  ]

  allowed_oauth_scopes = var.oauth_scopes
}

# ------------------------------------------------------------------
# Create Secrets Manager secret with Cognito client credentials
# ------------------------------------------------------------------
resource "aws_secretsmanager_secret" "api_client" {
  name        = "cognito-client-${var.client_name}"
  description = "Cognito client credentials for ${var.client_name}"

  tags = {
    Environment = var.client_name
  }
}

resource "aws_secretsmanager_secret_version" "api_client" {
  secret_id = aws_secretsmanager_secret.api_client.id
  secret_string = jsonencode({
    client_id     = aws_cognito_user_pool_client.api_client.id
    client_secret = aws_cognito_user_pool_client.api_client.client_secret
  })
}
