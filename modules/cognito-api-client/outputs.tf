output "cognito_client_id" {
  value = aws_cognito_user_pool_client.api_client.id
}

output "cognito_client_oauth_scopes" {
  value = aws_cognito_user_pool_client.api_client.allowed_oauth_scopes
}

output "secretsmanager_secret_arn" {
  value = aws_secretsmanager_secret.api_client.arn
}
