# ------------------------------------------------------------------
# Create serverless Aurora Cluster
# ------------------------------------------------------------------
resource "aws_db_subnet_group" "rds_aurora_serverless" {
  name       = "${var.env_prefix}-aurora-serverless"
  subnet_ids = var.subnet_ids

  tags = {
    Environment = var.env_prefix
  }
}

resource "aws_rds_cluster" "rds_aurora_serverless" {
  apply_immediately       = var.apply_immediately
  backup_retention_period = var.backup_retention_period
  cluster_identifier      = "${var.env_prefix}-aurora-serverless"
  copy_tags_to_snapshot   = true
  enable_http_endpoint    = var.data_api
  engine                  = var.engine
  engine_mode             = "serverless"
  database_name           = var.database_name
  master_username         = var.master_db_user
  master_password         = var.master_db_password
  deletion_protection     = true
  vpc_security_group_ids  = concat([aws_security_group.rds_server.id], var.security_group_ids)
  db_subnet_group_name    = aws_db_subnet_group.rds_aurora_serverless.name
  final_snapshot_identifier = "ci-aurora-cluster-backup"
  skip_final_snapshot     = true
  
  scaling_configuration {
    auto_pause               = var.auto_pause
    max_capacity             = var.max_capacity
    min_capacity             = var.min_capacity
    seconds_until_auto_pause = var.seconds_until_auto_pause
    timeout_action           = var.timeout_action
  }

  tags = {
    Environment = var.env_prefix
  }
}

# ------------------------------------------------------------------
# Create Security Groups for accessing Aurora Cluster
# ------------------------------------------------------------------
data "aws_subnet" "first" {
  id = var.subnet_ids[0]
}

resource "aws_security_group" "rds_server" {
  name        = "${var.env_prefix}-rds-server"
  description = "Controls access to RDS Aurora Cluster"
  vpc_id      = data.aws_subnet.first.vpc_id

  ingress {
    from_port       = var.engine == "aurora-postgresql" ? 5432 : 3306
    to_port         = var.engine == "aurora-postgresql" ? 5432 : 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.rds_client.id]
  }

  tags = {
    Environment = var.env_prefix
  }
}

resource "aws_security_group" "rds_client" {
  name        = "${var.env_prefix}-rds-client"
  description = "Allows client instances to access RDS Aurora Cluster"
  vpc_id      = data.aws_subnet.first.vpc_id

  tags = {
    Environment = var.env_prefix
  }
}

resource "aws_security_group_rule" "rds_client_limited_egress" {
  # always allow limited egress for rds clients
  type                     = "egress"
  from_port                = var.engine == "aurora-postgresql" ? 5432 : 3306
  to_port                  = var.engine == "aurora-postgresql" ? 5432 : 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.rds_server.id
  security_group_id        = aws_security_group.rds_client.id
}

resource "aws_security_group_rule" "rds_client_full_egress" {
  # allow all egress for rds clients if allow_client_outbound_traffic is true
  count = var.allow_client_outbound_traffic == true ? 1 : 0

  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.rds_client.id
}

# ------------------------------------------------------------------
# Create Secrets Manager secret with database credentials
# ------------------------------------------------------------------
resource "aws_secretsmanager_secret" "rds_aurora_serverless" {
  name        = "${var.env_prefix}-aurora-serverless"
  description = "RDS Aurora database credentials for ${var.env_prefix}"
  recovery_window_in_days = 0
  tags = {
    Environment = var.env_prefix
  }
}

resource "aws_secretsmanager_secret_version" "rds_aurora_serverless" {
  secret_id = aws_secretsmanager_secret.rds_aurora_serverless.id
  secret_string = jsonencode({
    dbClusterIdentifier = aws_rds_cluster.rds_aurora_serverless.cluster_identifier
    engine              = aws_rds_cluster.rds_aurora_serverless.engine
    host                = aws_rds_cluster.rds_aurora_serverless.endpoint
    port                = aws_rds_cluster.rds_aurora_serverless.port
    username            = aws_rds_cluster.rds_aurora_serverless.master_username
    password            = aws_rds_cluster.rds_aurora_serverless.master_password
  })
}