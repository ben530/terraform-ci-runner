# ------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ------------------------------------------------------------------
variable "allow_client_outbound_traffic" {
  type        = bool
  default     = false
  description = "Whether to allow other outbound traffic for the client security group (Defaults to false)"
}

variable "apply_immediately" {
  type        = bool
  default     = false
  description = "Whether to apply changes to the RDS cluster immediately, please note this will lead to cluster downtime (Defaults to false)"
}

variable "auto_pause" {
  type        = bool
  default     = true
  description = "Whether to enable automatic pause of the database when idle (Defaults to true)"
}

variable "backup_retention_period" {
  type        = number
  default     = 7
  description = "The days to retain backups for (Defaults to 7)"
}

variable "data_api" {
  type        = bool
  default     = false
  description = "Whether to enable HTTP endpoint for Aurora Serverless Data API (Defaults to false)"
}

variable "database_name" {
  type        = string
  description = "Name for the database"
}

variable "engine" {
  type        = string
  default     = "aurora-postgresql"
  description = "Database engine to use (Defaults to `aurora-postgresql`, also supports `aurora-mysql`)"
}

variable "env_prefix" {
  type        = string
  description = "Prefix used to namespace resources"
}

variable "master_db_user" {
  type        = string
  description = "Master username for the database"
}

variable "master_db_password" {
  type        = string
  description = "Master password for the database"
}

variable "max_capacity" {
  type        = number
  default     = 2
  description = "Maximum number of Aurora capacity units (ACUs) to use for the database (Defaults to 2)"
}

variable "min_capacity" {
  type        = number
  default     = 2
  description = "Minimum number of Aurora capacity units (ACUs) to use for the database (Defaults to 2)"
}

variable "seconds_until_auto_pause" {
  type        = number
  default     = 3600
  description = "The time, in seconds, before the database will be paused when idle (Defaults to 3600)"
}

variable "security_group_ids" {
  type        = list(string)
  default     = []
  description = "List of VPC Security Group Ids to grant access to the database"
}

variable "subnet_ids" {
  type        = list(string)
  description = "List of VPC Subnet Ids to use for database network interfaces"
}

variable "timeout_action" {
  type        = string
  default     = "RollbackCapacityChange"
  description = "The action to take when the timeout is reached. (Defaults to `RollbackCapacityChange`, also supports `ForceApplyCapacityChange`)"
}