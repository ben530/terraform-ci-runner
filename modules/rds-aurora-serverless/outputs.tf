output "rds_client_security_group_id" {
  value = aws_security_group.rds_client.id
}

output "rds_cluster_arn" {
  value = aws_rds_cluster.rds_aurora_serverless.arn
}

output "rds_cluster_database_name" {
  value = aws_rds_cluster.rds_aurora_serverless.database_name
}

output "rds_cluster_endpoint" {
  value = aws_rds_cluster.rds_aurora_serverless.endpoint
}

output "rds_cluster_maintenance_window" {
  value = aws_rds_cluster.rds_aurora_serverless.preferred_maintenance_window
}

output "rds_cluster_master_username" {
  value = aws_rds_cluster.rds_aurora_serverless.master_username
}

output "rds_cluster_port" {
  value = aws_rds_cluster.rds_aurora_serverless.port
}

output "rds_cluster_identifier" {
  value = aws_rds_cluster.rds_aurora_serverless.cluster_identifier
}

output "secretsmanager_secret_arn" {
  value = aws_secretsmanager_secret.rds_aurora_serverless.arn
}