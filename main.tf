# ------------------------------------------------------------------
# Terraform Backend and Provider configs
# ------------------------------------------------------------------
terraform {
  backend "s3" {}
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.7.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 2.2.0"
    }
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Managed = "terraform"
    }
  }
}

provider "random" {}

# ------------------------------------------------------------------
# Terraform Data Source 
# ------------------------------------------------------------------
data "aws_cognito_user_pools" "nea" {
  name = "nea-${var.stage}-pool"
}

# ------------------------------------------------
# Cognito Client for UI
# ------------------------------------------------
module "cognito-ui-client" {
  source               = "./modules/cognito-ui-client"
  client_name          = "${var.app_name}-ui-${var.stage}"
  cognito_user_pool_id = tolist(data.aws_cognito_user_pools.nea.ids)[0]
  callback_urls        = var.stage == "prod" ? ["https://reference-ui.nexteraanalytics.com/login"] : ["https://reference-ui.dev.nexteraanalytics.com/login", "http://localhost:8080/login"]
  logout_urls          = var.stage == "prod" ? ["https://reference-ui.nexteraanalytics.com/logout"] : ["https://reference-ui.dev.nexteraanalytics.com/logout", "http://localhost:8080/logout"]
}

# ------------------------------------------------
# Cognito Clients for API
# ------------------------------------------------
module "cognito-api-client-read" {
  source               = "./modules/cognito-api-client"
  client_name          = "${var.app_name}-api-${var.stage}-read"
  cognito_user_pool_id = tolist(data.aws_cognito_user_pools.nea.ids)[0]
  oauth_scopes         = ["${aws_cognito_resource_server.api.identifier}/read"]
}

module "cognito-api-client-write" {
  source               = "./modules/cognito-api-client"
  client_name          = "${var.app_name}-api-${var.stage}-write"
  cognito_user_pool_id = tolist(data.aws_cognito_user_pools.nea.ids)[0]
  oauth_scopes         = ["${aws_cognito_resource_server.api.identifier}/read", "${aws_cognito_resource_server.api.identifier}/write"]
}

# ------------------------------------------------
# Cognito Resource Server for API scopes
# ------------------------------------------------
resource "aws_cognito_resource_server" "api" {
  identifier = "${var.app_name}"
  name       = "${var.app_name}"

  scope {
    scope_name        = "read"
    scope_description = "read-only access to the API"
  }

  scope {
    scope_name        = "write"
    scope_description = "write access to the API"
  }

  user_pool_id = tolist(data.aws_cognito_user_pools.nea.ids)[0]
}

# ------------------------------------------------
# RDS Aurora Serverless db for API
# ------------------------------------------------
# Generate a random master password for the RDS database
resource "random_password" "db_password" {
  length           = 16
  special          = true
  override_special = "!#$&*()-_=+[]{}<>:?" # DO-1018 - avoid problematic special characters in passwords (@ and %)
}

# Look up the VPC and private Subnets to use for RDS
data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["clouddev", "WindLogics-JB-Prod"]
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.vpc.id

  filter {
    name   = "tag:Name"
    values = ["Clouddev-Private*", "WL-JB-Prod-Private*"]
  }
}

module "rds-aurora-serverless" {
  source                        = "./modules/rds-aurora-serverless"
  engine                        = "aurora-postgresql"
  database_name                 = replace(var.app_name, "-", "_")
  env_prefix                    = "${var.app_name}-${var.stage}"
  master_db_user                = replace(var.app_name, "-", "_")
  master_db_password            = random_password.db_password.result
  subnet_ids                    = tolist(data.aws_subnet_ids.private.ids)
  allow_client_outbound_traffic = true
}
